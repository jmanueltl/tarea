package com.curso.project.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.curso.project.model.Curso;

public interface ICursoDAO extends JpaRepository<Curso, Integer> {

}
