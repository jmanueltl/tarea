package com.curso.project.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.curso.project.model.Curso;
import com.curso.project.service.ICursoService;

@RestController
@RequestMapping(value = "/cursos")
public class CursoController {

	@Autowired
	private ICursoService service;
	
	@GetMapping
	public List<Curso> listar(){
		return service.listar();
	}
	
	@GetMapping(value = "/{id}")
	public Curso listarPorId(@PathVariable("id") Integer id){
		Optional<Curso> cur = service.listarPorId(id);
		return cur.isPresent() ? cur.get() : new Curso();
	}
	
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Curso registrar(@RequestBody Curso curso) {
		return service.registrar(curso);
	}

	@PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public Curso modificar(@RequestBody Curso curso) {
		return service.modificar(curso);
	}

	@DeleteMapping(value = "/{id}")
	public Integer eliminar(@PathVariable("id") Integer id) {
		Optional<Curso> opt = service.listarPorId(id);
		if (opt.isPresent()) {
			Curso cur = new Curso();
			cur.setId(id);
			service.eliminar(cur);
			return 1;
		}
		return 0;
	}
	
}
