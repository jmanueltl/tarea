package com.curso.project.service;

import java.util.List;
import java.util.Optional;

import com.curso.project.model.Curso;

public interface ICursoService {

	List<Curso> listar();
	Optional<Curso> listarPorId(Integer id);	
	Curso registrar(Curso curso);
	Curso modificar(Curso curso);
	void eliminar(Curso curso);
}
