package com.curso.project.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.curso.project.dao.ICursoDAO;
import com.curso.project.model.Curso;

@Service
public class CursoServiceImpl implements ICursoService{

	@Autowired
	private ICursoDAO dao;
	
	@Override
	public List<Curso> listar() {
		return dao.findAll();
	}

	@Override
	public Optional<Curso> listarPorId(Integer id) {		
		return dao.findById(id);
	}

	@Override
	public Curso registrar(Curso cur) {
		return dao.save(cur);
	}

	@Override
	public Curso modificar(Curso cur) {
		Optional<Curso> curso = dao.findById(cur.getId());
		
		if (curso.isPresent()) {
			return dao.save(cur);
		}
		return new Curso();
	}

	@Override
	public void eliminar(Curso cur) {
		dao.delete(cur);

	} 
}
